﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess
{
    public class DBClient : IDisposable
    {
        protected SqlConnection _connection;

        public DBClient(string connectionString)
        {
            _connection = CreateConnection(connectionString);
        }

        public SqlConnection GetConnection()
        {
            return _connection;
        }

        public List<T> GetData<T>(string queryString)
        {
            _connection.Open();
            var queryResult = _connection.Query<T>(queryString).ToList();
            _connection.Close();
            return queryResult;
        }

        //public List<TReturn> GetData<TFirst, TSecond, TReturn>(string queryString, Func<TFirst, TSecond, TReturn> map, string splitOn = "Id", object param = null)
        //{
        //    _connection.Open();
        //    var queryResult = _connection.Query<TFirst, TSecond, TReturn>(queryString, map, splitOn: splitOn, param: param).ToList();
        //    _connection.Close();
        //    return queryResult;
        //}

        public List<T> GetData<T>(string queryString, object param)
        {
            _connection.Open();
            var queryResult = _connection.Query<T>(queryString, param).ToList();
            _connection.Close();
            return queryResult;
        }

        //public List<T> GetData<T>(string queryString, DbTransaction transaction)
        //{
        //    var queryResult = _connection.Query<T>(queryString, null, transaction).ToList();
        //    return queryResult;
        //}

        //public List<T> GetData<T>(string queryString, DbTransaction transaction, object data)
        //{
        //    var queryResult = _connection.Query<T>(queryString, data, transaction).ToList();
        //    return queryResult;
        //}

        //public T GetScalar<T>(string queryString)
        //{
        //    _connection.Open();
        //    var queryResult = _connection.ExecuteScalar<T>(queryString);
        //    _connection.Close();
        //    return queryResult;
        //}

        //public T GetScalar<T>(string queryString, object param)
        //{
        //    _connection.Open();
        //    var queryResult = _connection.ExecuteScalar<T>(queryString, param);
        //    _connection.Close();
        //    return queryResult;
        //}

        //public T GetScalar<T>(string queryString, DbTransaction transaction)
        //{
        //    var queryResult = _connection.ExecuteScalar<T>(queryString, null, transaction);
        //    return queryResult;
        //}

        //public T GetScalar<T>(string queryString, DbTransaction transaction, object data)
        //{
        //    var queryResult = _connection.ExecuteScalar<T>(queryString, data, transaction);
        //    return queryResult;
        //}

        public T ExecuteComandWithSingleResult<T>(string queryString, object data)
        {
            _connection.Open();
            var result = _connection.QuerySingleOrDefault<T>(queryString, data);
            _connection.Close();
            return result;
        }

        //public int ExecuteCommand(string queryString)
        //{
        //    _connection.Open();
        //    var rowsAffected = _connection.Execute(queryString);
        //    _connection.Close();
        //    return rowsAffected;
        //}

        public int ExecuteCommand(string queryString, object param)
        {
            _connection.Open();
            var rowsAffected = _connection.Execute(queryString, param);
            _connection.Close();
            return rowsAffected;
        }

        //public int ExecuteCommand(string queryString, DbTransaction transaction, object data)
        //{
        //    var rowsAffected = _connection.Execute(queryString, data, transaction);
        //    return rowsAffected;
        //}

        //public int ExecuteCommand(string queryString, DbTransaction transaction)
        //{
        //    var rowsAffected = _connection.Execute(queryString, null, transaction);
        //    return rowsAffected;
        //}

        //public IEnumerable<T> ExecuteCommandWithReturn<T>(string queryString)
        //{
        //    _connection.Open();
        //    var result = _connection.Query<T>(queryString);
        //    _connection.Close();
        //    return result;
        //}

        public IEnumerable<T> ExecuteCommandWithReturn<T>(string queryString, object param)
        {
            _connection.Open();
            var result = _connection.Query<T>(queryString, param);
            _connection.Close();
            return result;
        }

        //public IEnumerable<T> ExecuteCommandWithReturn<T>(string queryString, DbTransaction transaction, object data)
        //{
        //    return _connection.Query<T>(queryString, data, transaction);
        //}

        //public IEnumerable<T> ExecuteCommandWithReturn<T>(string queryString, DbTransaction transaction)
        //{
        //    return _connection.Query<T>(queryString, null, transaction);
        //}

        public DbTransaction BeginTransaction()
        {
            _connection.Open();
            return _connection.BeginTransaction();

        }
        public void Commit(DbTransaction transaction)
        {
            transaction.Commit();
            _connection.Close();
        }

        public void Dispose()
        {
            _connection.Dispose();
        }

        private SqlConnection CreateConnection(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException("Cannot find connection string in config file!");
            }
            try
            {
                return new SqlConnection(connectionString);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
