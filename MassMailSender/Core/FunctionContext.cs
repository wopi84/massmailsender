﻿using System;

namespace Core
{
    public  class FunctionContext
    {
        public static T GetEnvironmentVariable<T>(string variable)
        {
            var value = Environment.GetEnvironmentVariable(variable.ToString(), EnvironmentVariableTarget.Process)
                        ?? throw new ArgumentNullException(variable.ToString());
            return (T)Convert.ChangeType(value, typeof(T));
        }

        public static string GetEnvironmentVariable(string variable)
        {
            return GetEnvironmentVariable<string>(variable);
        }
    }
}
