﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Core
{
    public static class Retry
    {
        public static T For<T>(Func<T> caller, int retryCount, TimeSpan delay)
        {
            var exceptions = new List<Exception>();

            for (var i = 0; i < retryCount; i++)
            {
                try
                {
                    return caller();
                }
                catch (Exception ex)
                {
                    Thread.Sleep(delay);
                    exceptions.Add(ex);
                }
            }

            throw new AggregateException(exceptions);
        }
    }
}
