﻿using System.Composition;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Core;
using Dtos.Response;

namespace Services
{
    [Export(typeof(IMailSenderService))]
    public class MailSenderService : IMailSenderService
    {
        private readonly SmtpClient _client;

        public MailSenderService()
        {
            var server = FunctionContext.GetEnvironmentVariable("SmtpServer");
            _client = new SmtpClient(server);
            var username = FunctionContext.GetEnvironmentVariable("SmtpUsername");
            var password = FunctionContext.GetEnvironmentVariable("SmtpPassword");
            _client.Credentials = new NetworkCredential(username, password);
        }

        public Task SendMailAsync(GetDetailsResponse data)
        {
            var from = new MailAddress(data.SenderMail, data.SenderName);
            var mailMessage = new MailMessage();
            mailMessage.From = from;
            mailMessage.Subject = data.Subject;
            mailMessage.Body = data.Body;
            mailMessage.Priority = data.MailPriority;
            foreach (var recipient in data.Recipients)
            {
                var to = new MailAddress(recipient.RecipientMail, recipient.RecipientName);
                mailMessage.To.Add(to);
            }

            return _client.SendMailAsync(mailMessage);
        }
    }
}
