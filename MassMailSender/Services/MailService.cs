﻿using Core;
using DataAccess;
using Dtos.Request;
using Dtos.Response;
using Exceptions;
using System;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.Net.Mail;

namespace Services
{
    [Export(typeof(IMailService))]
    public class MailService : IMailService
    {
        private readonly DBClient _dbClient;

        public MailService()
        {
            var connectionString = FunctionContext.GetEnvironmentVariable("SqlConnectionString");
            _dbClient = new DBClient(connectionString);
        }

        public AddeRecipientResponse AddeRecipient(Guid id, AddRecipientRequest addRecipientDto)
        {
            const string addRecipientSql = @"IF EXISTS(SELECT 1 FROM Mail WHERE Id = @MailId AND IsMailSend = 0)
                                                INSERT INTO [MailRecipient](MailId, RecipientName, RecipientMail)
                                                OUTPUT INSERTED.[Id]
                                                VALUES(@MailId, @RecipientName, @RecipientMail);";

            var guid = _dbClient.ExecuteComandWithSingleResult<Guid>(addRecipientSql, new { MailId = id, RecipientName = addRecipientDto.Name, RecipientMail = addRecipientDto.Email });
            if (guid == Guid.Empty)
            {
                throw new MailNotFoundException($"Mail with id='{id}' not found or IsMailSend=true");
            }
            var response = new AddeRecipientResponse { RecipientId = guid };
            return response;
        }

        public CreateMailResponse CreateMail(CreateMailRequest createMailDto)
        {
            const string createMailSql = @"INSERT INTO [Mail](Subject, Body, IsMailSend, CreationTime, ModifiedTime)
                        OUTPUT INSERTED.[Id]
                        VALUES(@Subject, @Body, @IsMailSend, @CreationTime, @ModifiedTime);";

            var now = DateTime.UtcNow;

            var guid = _dbClient.ExecuteComandWithSingleResult<Guid>(createMailSql, new { createMailDto.Subject, createMailDto.Body, IsMailSend = false, CreationTime = now, ModifiedTime = now });

            var response = new CreateMailResponse { MailId = guid };
            return response;
        }

        public GetDetailsResponse GetDetails(Guid id)
        {
            const string getDetailsSql = @"SELECT
                                            m.Id as MailId,
                                            m.Subject,
                                            m.Body,
                                            m.CreationTime,
                                            m.ModifiedTime,
                                            m.SenderName,
                                            m.SenderMail,
                                            m.MailPriority,
                                            r.Id as RecipientId,
                                            r.RecipientName,
                                            r.RecipientMail,
                                            CASE WHEN m.IsMailSend = 1 THEN 'sent' ELSE 'pending' END AS IsMailSend
                                           FROM [dbo].[Mail] m
                                           LEFT JOIN [dbo].[MailRecipient] r ON m.Id = r.MailId
                                           WHERE m.Id = @Id;";

            var response = _dbClient.GetData<dynamic>(getDetailsSql, new { Id = id })
                .GroupBy(x => x.MailId)
                .Select(x => new GetDetailsResponse
                {
                    MailId = x.Key,
                    Body = x.FirstOrDefault()?.Body,
                    CreationTime = x.First().CreationTime,
                    ModifiedTime = x.First().ModifiedTime,
                    SenderMail = x.FirstOrDefault()?.SenderMail,
                    Subject = x.FirstOrDefault()?.Subject,
                    SenderName = x.FirstOrDefault()?.SenderName,
                    IsMailSend = x.FirstOrDefault()?.IsMailSend,
                    MailPriority = (MailPriority)(x.FirstOrDefault()?.MailPriority),
                    Recipients = x.Select(r => new RecipientResponse { RecipientId = r.RecipientId, RecipientName = r.RecipientName, RecipientMail = r.RecipientMail }).ToList()

                })
                .SingleOrDefault();
            return response;
        }

        public List<GetDetailsResponse> GetAll()
        {
            const string getAllSql = @"SELECT
                                            m.Id as MailId,
                                            m.Subject,
                                            m.Body,
                                            m.CreationTime,
                                            m.ModifiedTime,
                                            m.SenderName,
                                            m.SenderMail,
                                            m.MailPriority,
                                            r.Id as RecipientId,
                                            r.RecipientName,
                                            r.RecipientMail,
                                            CASE WHEN m.IsMailSend = 1 THEN 'sent' ELSE 'pending' END AS IsMailSend
                                           FROM [dbo].[Mail] m
                                           LEFT JOIN [dbo].[MailRecipient] r ON m.Id = r.MailId;";

            var response = _dbClient.GetData<dynamic>(getAllSql)
                .GroupBy(x => x.MailId)
                .Select(x => new GetDetailsResponse
                {
                    MailId = x.Key,
                    Body = x.FirstOrDefault()?.Body,
                    CreationTime = x.First().CreationTime,
                    ModifiedTime = x.First().ModifiedTime,
                    SenderMail = x.FirstOrDefault()?.SenderMail,
                    Subject = x.FirstOrDefault()?.Subject,
                    SenderName = x.FirstOrDefault()?.SenderName,
                    IsMailSend = x.FirstOrDefault()?.IsMailSend,
                    MailPriority = (MailPriority)(x.FirstOrDefault()?.MailPriority),
                    Recipients = x.Select(r => new RecipientResponse { RecipientId = r.RecipientId, RecipientName = r.RecipientName, RecipientMail = r.RecipientMail }).ToList()
                })
                .ToList();
            return response;
        }

        public GetStatusResponse GetStatus(Guid id)
        {
            const string getStatusSql = @"SELECT IsMailSend FROM [dbo].[Mail]
                                           WHERE Id = @Id;";

            var isMailSend = _dbClient.ExecuteComandWithSingleResult<bool>(getStatusSql, new { Id = id });

            var response = new GetStatusResponse { MailStatus = isMailSend ? "sent" : "pending" };
            return response;
        }

        public void SetSender(Guid id, SetSenderRequest setSenderDto)
        {
            const string setSenderSql = @"IF EXISTS(SELECT 1 FROM Mail WHERE Id = @MailId AND IsMailSend = 0)
                                            UPDATE [dbo].[Mail]
                                            SET
                                                [SenderName] = @SenderName,
                                                [SenderMail] = @SenderMail,
                                                [ModifiedTime] = @ModifiedTime
                                            WHERE Id = @Id;";

            var now = DateTime.UtcNow;

            var rowsAffected = _dbClient.ExecuteCommand(setSenderSql, new { SenderName = setSenderDto.Name, SenderMail = setSenderDto.Email, Id = id, ModifiedTime = now });

            if (rowsAffected == 0)
            {
                throw new MailNotFoundException($"Mail with id='{id}' not found or IsMailSend=true");
            }
        }

        public void SetPriority(Guid id, SetPriorityRequest data)
        {
            const string setPrioritySql = @"IF EXISTS(SELECT 1 FROM Mail WHERE Id = @MailId AND IsMailSend = 0)
                                                UPDATE [dbo].[Mail]
                                                SET
                                                    [MailPriority] = @Priority,
                                                    [ModifiedTime] = @ModifiedTime
                                                WHERE Id = @Id;";

            var now = DateTime.UtcNow;

            var rowsAffected = _dbClient.ExecuteCommand(setPrioritySql, new { Priority = (int)data.MailPriority, Id = id, ModifiedTime = now });

            if (rowsAffected == 0)
            {
                throw new MailNotFoundException($"Mail with id='{id}' not found or IsMailSend=true");
            }
        }

        public SendAllResponse SendAll()
        {
            const string sendAllResponseSql = @"UPDATE m
                                                SET 
                                                    m.[IsMailSend] = 1,
                                                    m.[ModifiedTime] = @ModifiedTime
                                                OUTPUT inserted.[ID]
                                                FROM [dbo].[Mail] m
                                                LEFT JOIN [dbo].[MailRecipient] r ON m.Id = r.MailId
                                                WHERE m.[IsMailSend] = 0 AND m.SenderMail IS NOT NULL AND r.RecipientMail IS NOT NULL;";
            var now = DateTime.UtcNow;


            var ids = _dbClient.ExecuteCommandWithReturn<Guid>(sendAllResponseSql, new { ModifiedTime = now }).ToList();

            var response = new SendAllResponse { MailToSend = ids };
            return response;
        }
    }
}
