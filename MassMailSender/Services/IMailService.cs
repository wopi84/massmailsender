﻿using Dtos.Request;
using Dtos.Response;
using System;
using System.Collections.Generic;

namespace Services
{
    public interface IMailService
    {
        CreateMailResponse CreateMail(CreateMailRequest createMailDto);
        void SetSender(Guid id, SetSenderRequest data);
        GetStatusResponse GetStatus(Guid id);
        AddeRecipientResponse AddeRecipient(Guid id, AddRecipientRequest data);
        GetDetailsResponse GetDetails(Guid id);
        List<GetDetailsResponse> GetAll();
        void SetPriority(Guid id, SetPriorityRequest data);
        SendAllResponse SendAll();
    }
}
