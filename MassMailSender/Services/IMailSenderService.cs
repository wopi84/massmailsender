﻿using Dtos.Response;
using System.Threading.Tasks;

namespace Services
{
    public interface IMailSenderService
    {
        Task SendMailAsync(GetDetailsResponse data);
    }
}
