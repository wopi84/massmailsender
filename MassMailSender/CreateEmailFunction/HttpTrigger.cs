using Dtos.Request;
using Exceptions;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Services;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using Validators;

namespace CreateEmailFunction
{
    public static class HttpTrigger
    {
        [FunctionName(nameof(CreateMail))]
        public static async Task<HttpResponseMessage> CreateMail(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "mails")] HttpRequestMessage req,
            ILogger logger)
        {
            logger.LogInformation($"C# {nameof(CreateMail)} function processed a request.");

            using (var container = Bootstrapper.CreateContainer(logger))
            {
                var mailCreateService = container.GetExport<IMailService>();
                var data = await req.Content.ReadAsAsync<CreateMailRequest>();
                var validator = container.GetExport<IValidator<CreateMailRequest>>();
                if (validator.IsValid(data))
                {
                    var response = mailCreateService.CreateMail(data);
                    return req.CreateResponse(HttpStatusCode.Created, response, JsonMediaTypeFormatter.DefaultMediaType);
                }
                else
                {
                    return req.CreateResponse(HttpStatusCode.BadRequest, JsonMediaTypeFormatter.DefaultMediaType);
                }
            }
        }

        [FunctionName(nameof(AddRecipient))]
        public static async Task<HttpResponseMessage> AddRecipient(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "mails/{id}/recipient")] HttpRequestMessage req,
            Guid id,
            ILogger logger)
        {
            logger.LogInformation($"C# {nameof(AddRecipient)} function processed a request.");

            using (var container = Bootstrapper.CreateContainer(logger))
            {
                var mailCreateService = container.GetExport<IMailService>();
                var data = await req.Content.ReadAsAsync<AddRecipientRequest>();
                var validator = container.GetExport<IValidator<AddRecipientRequest>>();
                if (validator.IsValid(data))
                {
                    try
                    {
                        var response = mailCreateService.AddeRecipient(id, data);
                        return req.CreateResponse(HttpStatusCode.Created, response, JsonMediaTypeFormatter.DefaultMediaType);
                    }
                    catch (MailNotFoundException ex)
                    {
                        logger.LogError(ex, $"{nameof(AddRecipient)} throws exception.");

                        return req.CreateResponse(HttpStatusCode.NotFound, JsonMediaTypeFormatter.DefaultMediaType);
                    }
                }
                else
                {
                    return req.CreateResponse(HttpStatusCode.BadRequest, JsonMediaTypeFormatter.DefaultMediaType);
                }
            }
        }

        [FunctionName(nameof(AddSender))]
        public static async Task<HttpResponseMessage> AddSender(
            [HttpTrigger(AuthorizationLevel.Function, "put", Route = "mails/{id}")] HttpRequestMessage req,
            Guid id,
            ILogger logger)
        {
            logger.LogInformation($"C# {nameof(AddSender)} function processed a request.");

            using (var container = Bootstrapper.CreateContainer(logger))
            {
                var mailCreateService = container.GetExport<IMailService>();
                var data = await req.Content.ReadAsAsync<SetSenderRequest>();
                var validator = container.GetExport<IValidator<SetSenderRequest>>();
                if (validator.IsValid(data))
                {
                    try
                    {
                        mailCreateService.SetSender(id, data);
                        return req.CreateResponse(HttpStatusCode.NoContent);
                    }
                    catch (MailNotFoundException ex)
                    {
                        logger.LogError(ex, $"{nameof(AddSender)} throws exception.");

                        return req.CreateResponse(HttpStatusCode.NotFound, JsonMediaTypeFormatter.DefaultMediaType);
                    }
                }
                else
                {
                    return req.CreateResponse(HttpStatusCode.BadRequest, JsonMediaTypeFormatter.DefaultMediaType);
                }
            }
        }

        [FunctionName(nameof(GetStatus))]
        public static async Task<HttpResponseMessage> GetStatus(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "mails/{id}/status")] HttpRequestMessage req,
            Guid id,
            ILogger logger)
        {
            logger.LogInformation($"C# {nameof(GetStatus)} function processed a request.");

            using (var container = Bootstrapper.CreateContainer(logger))
            {
                var mailCreateService = container.GetExport<IMailService>();
                var response = mailCreateService.GetStatus(id);
                return req.CreateResponse(HttpStatusCode.OK, response, JsonMediaTypeFormatter.DefaultMediaType);
            }
        }

        [FunctionName(nameof(GetDetails))]
        public static async Task<HttpResponseMessage> GetDetails(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "mails/{id}")] HttpRequestMessage req,
            Guid id,
            ILogger logger)
        {
            logger.LogInformation($"C# {nameof(GetDetails)} function processed a request.");

            using (var container = Bootstrapper.CreateContainer(logger))
            {
                var mailCreateService = container.GetExport<IMailService>();
                var response = mailCreateService.GetDetails(id);
                return req.CreateResponse(HttpStatusCode.OK, response, JsonMediaTypeFormatter.DefaultMediaType);
            }
        }

        [FunctionName(nameof(GetAll))]
        public static async Task<HttpResponseMessage> GetAll(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "mails")] HttpRequestMessage req,
            ILogger logger)
        {
            logger.LogInformation($"C# {nameof(GetAll)} function processed a request.");

            using (var container = Bootstrapper.CreateContainer(logger))
            {
                var mailCreateService = container.GetExport<IMailService>();
                var response = mailCreateService.GetAll();
                return req.CreateResponse(HttpStatusCode.OK, response, JsonMediaTypeFormatter.DefaultMediaType);
            }
        }

        [FunctionName(nameof(SendAll))]
        public static async Task<HttpResponseMessage> SendAll(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "mails/sendAll")] HttpRequestMessage req,
            [Queue("mail-to-send", Connection = "AzureWebJobsStorage")] ICollector<string> queue,
            ILogger logger)
        {
            logger.LogInformation($"C# {nameof(SendAll)} function processed a request.");

            using (var container = Bootstrapper.CreateContainer(logger))
            {
                var mailCreateService = container.GetExport<IMailService>();
                var response = mailCreateService.SendAll();
                foreach (var item in response.MailToSend)
                {
                    queue.Add(item.ToString());
                }
                return req.CreateResponse(HttpStatusCode.OK, response, JsonMediaTypeFormatter.DefaultMediaType);
            }
        }

        [FunctionName(nameof(SetPrioryty))]
        public static async Task<HttpResponseMessage> SetPrioryty(
            [HttpTrigger(AuthorizationLevel.Function, "put", Route = "mails/{id}/priority ")] HttpRequestMessage req,
            Guid id,
            ILogger logger)
        {
            using (var container = Bootstrapper.CreateContainer(logger))
            {
                var mailCreateService = container.GetExport<IMailService>();
                var data = await req.Content.ReadAsAsync<SetPriorityRequest>();
                try
                {
                    mailCreateService.SetPriority(id, data);
                    return req.CreateResponse(HttpStatusCode.NoContent, JsonMediaTypeFormatter.DefaultMediaType);
                }
                catch (MailNotFoundException ex)
                {
                    logger.LogError(ex, $"{nameof(SetPrioryty)} throws exception.");
                    return req.CreateResponse(HttpStatusCode.NotFound, JsonMediaTypeFormatter.DefaultMediaType);
                }
            }
        }
    }
}
