﻿using Core;
using Microsoft.Extensions.Logging;
using Services;
using System;
using System.Composition.Hosting;
using Validators;

namespace CreateEmailFunction
{
    internal class Bootstrapper
    {
        public static CompositionHost CreateContainer(ILogger logger)
        {
            return Retry.For(() => CreateContainerInternal(logger), 10, TimeSpan.FromSeconds(1));
        }

        private static CompositionHost CreateContainerInternal(ILogger logger)
        {
            var configuration = new ContainerConfiguration();
            configuration.WithAssembly(typeof(Bootstrapper).Assembly);
            configuration.WithAssembly(typeof(IMailService).Assembly);
            configuration.WithAssembly(typeof(IValidator<>).Assembly);

            return configuration.CreateContainer();
        }
    }
}
