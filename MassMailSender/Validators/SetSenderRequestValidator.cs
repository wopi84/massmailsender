﻿using Dtos.Request;
using System.Composition;

namespace Validators
{
    [Export(typeof(IValidator<SetSenderRequest>))]
    public class SetSenderRequestValidator : IValidator<SetSenderRequest>
    {
        public bool IsValid(SetSenderRequest data)
        {
            return !string.IsNullOrEmpty(data.Email);
        }
    }
}
