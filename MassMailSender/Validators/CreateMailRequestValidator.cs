﻿using Dtos.Request;
using System.Composition;

namespace Validators
{
    [Export(typeof(IValidator<CreateMailRequest>))]
    public class CreateMailRequestValidator : IValidator<CreateMailRequest>
    {
        public bool IsValid(CreateMailRequest data)
        {
            return !string.IsNullOrEmpty(data.Body) && !string.IsNullOrEmpty(data.Subject);
        }
    }
}
