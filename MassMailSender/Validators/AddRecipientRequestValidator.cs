﻿using Dtos.Request;
using System.Composition;

namespace Validators
{
    [Export(typeof(IValidator<AddRecipientRequest>))]

    public class AddRecipientRequestValidator : IValidator<AddRecipientRequest>
    {
        public bool IsValid(AddRecipientRequest data)
        {
            return !string.IsNullOrEmpty(data.Email);
        }
    }
}
