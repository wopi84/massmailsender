﻿namespace Validators
{
    public interface IValidator<T>
    {
        bool IsValid(T data);
    }
}
