﻿using System;
using System.Collections.Generic;
using System.Net.Mail;

namespace Dtos.Response
{
    public class GetDetailsResponse
    {
        public Guid MailId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string SenderName { get; set; }
        public string SenderMail { get; set; }
        public string IsMailSend { get; set; }
        public MailPriority MailPriority { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime ModifiedTime { get; set; }

        public List<RecipientResponse> Recipients { get; set; }

        public GetDetailsResponse()
        {
            Recipients = new List<RecipientResponse>();
        }
    }
}
