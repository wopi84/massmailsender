﻿namespace Dtos.Response
{
    public class GetStatusResponse
    {
        public string MailStatus { get; set; }
    }
}
