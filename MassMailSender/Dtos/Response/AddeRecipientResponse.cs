﻿using System;

namespace Dtos.Response
{
    public class AddeRecipientResponse
    {
        public Guid RecipientId { get; set; }
    }
}
