﻿using System;

namespace Dtos.Response
{
    public class CreateMailResponse
    {
        public Guid MailId { get; set; }
    }
}
