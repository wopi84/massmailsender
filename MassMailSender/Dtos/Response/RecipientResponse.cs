﻿using System;

namespace Dtos.Response
{
    public class RecipientResponse
    {
        public Guid? RecipientId { get; set; }
        public string RecipientName { get; set; }
        public string RecipientMail { get; set; }
    }
}
