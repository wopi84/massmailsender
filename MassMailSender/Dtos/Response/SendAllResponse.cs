﻿using System;
using System.Collections.Generic;

namespace Dtos.Response
{
    public class SendAllResponse
    {
        public List<Guid> MailToSend { get; set; }
    }
}
