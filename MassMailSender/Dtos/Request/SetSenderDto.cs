﻿namespace Dtos.Request
{
    public class SetSenderRequest
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
