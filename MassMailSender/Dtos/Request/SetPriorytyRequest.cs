﻿using System.Net.Mail;

namespace Dtos.Request
{
    public class SetPriorityRequest
    {
        public MailPriority MailPriority { get; set; }
    }
}
