﻿namespace Dtos.Request
{
    public class AddRecipientRequest
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}

