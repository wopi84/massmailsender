﻿namespace Dtos.Request
{
    public class CreateMailRequest
    {
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
