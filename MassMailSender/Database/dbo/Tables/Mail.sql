﻿CREATE TABLE [dbo].[Mail] (
    [Id]           UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [Subject]      NVARCHAR (150)   NULL,
    [Body]         NVARCHAR (MAX)   NULL,
    [SenderName]   NVARCHAR (50)    NULL,
    [SenderMail]   NVARCHAR (50)    NULL,
    [IsMailSend]   BIT              NOT NULL,
    [CreationTime] DATETIME         NOT NULL,
    [ModifiedTime] DATETIME         NOT NULL,
    [MailPriority] INT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Mail] PRIMARY KEY CLUSTERED ([Id] ASC)
);

