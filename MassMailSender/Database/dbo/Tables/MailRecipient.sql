﻿CREATE TABLE [dbo].[MailRecipient] (
    [Id]            UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [MailId]        UNIQUEIDENTIFIER NOT NULL,
    [RecipientName] NVARCHAR (50)    NULL,
    [RecipientMail] NVARCHAR (50)    NOT NULL,
    CONSTRAINT [PK_MailRecipient] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_MailRecipient_Mail] FOREIGN KEY ([MailId]) REFERENCES [dbo].[Mail] ([Id]),
    CONSTRAINT [FK_TempSales_MailRecipient] FOREIGN KEY ([MailId]) REFERENCES [dbo].[Mail] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE
);

