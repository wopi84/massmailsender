﻿using Microsoft.WindowsAzure.Storage.Table;
using System;

namespace Mail.Data
{
    public class MailEntity : TableEntity, ITableIdentity
    {
        public Guid Id { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public int MyProperty { get; set; }
    }

}
