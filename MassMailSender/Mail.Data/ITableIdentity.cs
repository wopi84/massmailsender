﻿using System;

namespace Mail.Data
{
    interface ITableIdentity
    {
        Guid Id { get; set; }
    }
}
