﻿using System;

namespace Exceptions
{
    [Serializable]
    public class MailNotFoundException: Exception
    {
        public MailNotFoundException()
        {
        }

        public MailNotFoundException(string message)
            : base(message)
        {
        }

        public MailNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
