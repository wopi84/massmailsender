using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Services;
using System;
using System.Threading.Tasks;

namespace SendMailFunction
{
    public static class SendMailFromQueue
    {
        [FunctionName(nameof(SendMailFromQueue))]
        public static async Task Run([QueueTrigger("mail-to-send", Connection = "AzureWebJobsStorage")]string queue, 
            ILogger logger)
        {
            logger.LogInformation($"{nameof(SendMailFromQueue)} started processing {queue}.");

            using (var container = Bootstrapper.CreateContainer(logger))
            {
                var mailCreateService = container.GetExport<IMailService>();
                var mailSenderService = container.GetExport<IMailSenderService>();
                try
                {
                    var id = new Guid(queue);
                    var mailDetails = mailCreateService.GetDetails(id);
                    await mailSenderService.SendMailAsync(mailDetails);
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, $"{nameof(SendMailFromQueue)} throws exception.");
                }
            }
        }
    }
}
