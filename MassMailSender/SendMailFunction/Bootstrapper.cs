﻿using Core;
using Microsoft.Extensions.Logging;
using Services;
using System;
using System.Composition.Hosting;

namespace SendMailFunction
{
    internal class Bootstrapper
    {
        public static CompositionHost CreateContainer(ILogger logger)
        {
            return Retry.For(() => CreateContainerInternal(logger), 10, TimeSpan.FromSeconds(1));
        }

        private static CompositionHost CreateContainerInternal(ILogger logger)
        {
            var configuration = new ContainerConfiguration();
            configuration.WithAssembly(typeof(Bootstrapper).Assembly);
            configuration.WithAssembly(typeof(IMailService).Assembly);

            return configuration.CreateContainer();
        }
    }
}
